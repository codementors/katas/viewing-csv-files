package de.codementors.kata;

import java.util.List;

import static java.util.Collections.emptyList;

public class Paginator {

    private final int rowsPerPage;
    private final List<Row> rows;
    private int pageNumber = 1;

    public Paginator(List<Row> rows) {
        rowsPerPage = 3;
        this.rows = rows;
    }

    public List<Row> current() {
        if (rows == null) return emptyList();

        int from = (pageNumber - 1) * rowsPerPage;
        int to = pageNumber * this.rowsPerPage;

        if (rows.size() < to) to = rows.size();

        return rows.subList(from, to);
    }

    public boolean hasNext() {
        return rows.size() > pageNumber * this.rowsPerPage;
    }

    public List<Row> next() {
        if (hasNext()) pageNumber++;

        return current();
    }

    public boolean hasPrev() {
        return pageNumber - 1 > 0;
    }

    public List<Row> prev() {
        if (hasPrev()) pageNumber--;

        return current();
    }
}
