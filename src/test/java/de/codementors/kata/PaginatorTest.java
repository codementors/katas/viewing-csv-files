package de.codementors.kata;


import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Lists.emptyList;

public class PaginatorTest {

    private final Row row1 = new Row();
    private final Row row2 = new Row();
    private final Row row3 = new Row();
    private final Row row4 = new Row();

    @Test
    public void givenNothing_whenCurrent_shouldReturnEmptyList() {
        Paginator paginator = new Paginator(emptyList());

        assertThat(paginator.current()).isEqualTo(emptyList());
    }

    @Test
    void givenOneRow_whenCurrent_shouldReturnSinglePageWithOneRow() {
        Paginator paginator = new Paginator(List.of(row1));

        assertThat(paginator.current()).isEqualTo(List.of(row1));
    }

    @Test
    void given4Rows_whenCurrent_shouldReturnThreeEntriesOnFirstPage() {
        Paginator paginator = new Paginator(List.of(row1, row2, row3, row4));

        assertThat(paginator.current()).isEqualTo(List.of(row1, row2, row3));
    }

    @Test
    void given4Rows_whenHasNext_shouldReturnTrue() {
        Paginator paginator = new Paginator(List.of(row1, row2, row3, row4));

        assertThat(paginator.hasNext()).isTrue();
    }

    @Test
    void given4RowsOnSecondPage_whenHasNext_shouldReturnTrue() {
        Paginator paginator = new Paginator(List.of(row1, row2, row3, row4));

        assertThat(paginator.hasNext()).isTrue();
    }

    @Test
    void given4Rows_whenNext_shouldReturnSecondPage() {
        Paginator paginator = new Paginator(List.of(row1, row2, row3, row4));

        assertThat(paginator.next()).isEqualTo(List.of(row4));
    }

    @Test
    void given4RowsAndSecondPage_whenHasPrev_shouldReturnTrue() {
        Paginator paginator = new Paginator(List.of(row1, row2, row3, row4));
        paginator.next();

        assertThat(paginator.hasPrev()).isTrue();
    }

    @Test
    void given4RowsAndSecondPage_whenPrev_shouldReturnFirstPage() {
        Paginator paginator = new Paginator(List.of(row1, row2, row3, row4));
        paginator.next();

        assertThat(paginator.prev()).isEqualTo(List.of(row1, row2, row3));
    }

}