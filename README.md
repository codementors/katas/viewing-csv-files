View CSV Files Kata
===================

Description:  
https://ccd-school.de/coding-dojo/agility-katas/viewing-csv-files-i/

## Solution Idea
  
```plantuml
@startuml component
title Solution Idea 
actor User
node ConsoleController
node Paginator
node Rows
node CSVFileAdapter
node ConsolePresenter

User - ConsoleController
ConsoleController - Paginator
Paginator - CSVFileAdapter
ConsolePresenter -u- Paginator
Rows -u- Paginator

legend
    |= Component |= Description |
    | ConsoleController | Take input from the console and request the actions to the interactor (Paginator) |
    | Paginator | Interacts with the entities (Rows / List)|
    | CSVFileAdapter | Reads CSV from file |
    | ConsolePresenter | Presents the result to the console |
    | Rows | Entity |
endlegend
@enduml
```